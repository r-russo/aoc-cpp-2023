#include <iostream>
#include <sstream>
#include <vector>

#include "utils.h"

typedef std::vector<std::vector<int>> dataset_t;

dataset_t parse_input(const std::string &input) {
    std::stringstream in(input);
    dataset_t dataset{};

    for (std::string line; std::getline(in, line);) {
        std::vector<int> history{};
        std::stringstream ln(line);
        for (std::string field; std::getline(ln, field, ' ');) {
            history.push_back(std::stoi(field));
        }
        dataset.push_back(history);
    }

    return dataset;
}

int extrapolate_last(const std::vector<int> &history) {
    bool done = true;
    for (const int &v : history) {
        if (v != 0) {
            done = false;
            break;
        }
    }
    if (done) {
        return 0;
    }

    std::vector<int> dif(history.size() - 1);
    for (size_t i = 0; i < history.size() - 1; i++) {
        dif[i] = history[i + 1] - history[i];
    }

    return extrapolate_last(dif) + dif.back();
}

int extrapolate_first(const std::vector<int> &history) {
    bool done = true;
    for (const int &v : history) {
        if (v != 0) {
            done = false;
            break;
        }
    }
    if (done) {
        return 0;
    }

    std::vector<int> dif(history.size() - 1);
    for (size_t i = 0; i < history.size() - 1; i++) {
        dif[i] = history[i + 1] - history[i];
    }

    return dif.front() - extrapolate_first(dif);
}

int part1(std::string input) {
    dataset_t dataset = parse_input(input);
    int sum = 0;
    for (const auto &history : dataset) {
        sum += extrapolate_last(history) + history.back();
    }

    return sum;
}

int part2(std::string input) {
    dataset_t dataset = parse_input(input);
    int sum = 0;
    for (const auto &history : dataset) {
        int first = history.front() - extrapolate_first(history);
        sum += first;
    }

    return sum;
}

int main() {
//     std::string input = R"(0 3 6 9 12 15
// 1 3 6 10 15 21
// 10 13 16 21 30 45)";
    std::string input = read_day(9);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}