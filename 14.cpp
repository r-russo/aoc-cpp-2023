#include <algorithm>
#include <cstdint>
#include <iostream>
#include <set>
#include <sstream>
#include <vector>

#include "utils.h"

std::vector<std::string> parse_input(const std::string &input) {
    std::stringstream in(input);
    std::vector<std::string> dish{};
    for (std::string line; std::getline(in, line);) {
        dish.push_back(line);
    }
    return dish;
}

void tilt_north(std::vector<std::string> &dish) {
    for (size_t i = 0; i < dish.size(); i++) {
        std::string &row = dish[i];
        size_t last_rock_ix = 0;
        size_t rock_ix;
        while ((rock_ix = row.find("O", last_rock_ix)) != std::string::npos) {
            for (size_t j = i - 1; j != SIZE_MAX; j--) {
                if (dish[j][rock_ix] != '.') {
                    break;
                }
                dish[j][rock_ix] = 'O';
                dish[j + 1][rock_ix] = '.';
            }
            last_rock_ix = rock_ix + 1;
        }
    }
}

std::vector<std::string> rotate(const std::vector<std::string> &dish) {
    std::vector<std::string> new_dish(dish[0].size());
    for (size_t i = 0; i < new_dish.size(); i++) {
        std::string line{};
        for (size_t j = dish.size() - 1; j != SIZE_MAX; j--) {
            line += dish[j][i];
        }
        new_dish[i] = line;
    }

    return new_dish;
}

std::string get_state(const std::vector<std::string> &dish) {
    std::string state{};
    for (const std::string &line : dish) {
        state += line;
    }
    return state;
}

int calculate_load(const std::vector<std::string> &dish) {
    int sum = 0;
    for (size_t i = 0; i < dish.size(); i++) {
        int load = dish.size() - i;
        for (const char &c : dish[i]) {
            sum += static_cast<int>(c == 'O') * load;
        }
    }

    return sum;
}

int part1(std::string input) {
    std::vector<std::string> dish = parse_input(input);
    tilt_north(dish);
    return calculate_load(dish);
}

int part2(std::string input) {
    std::vector<std::string> dish = parse_input(input);
    std::set<std::string> visited_states{};
    std::vector<int> loads{};
    for (int i = 0; i < 1000000000; i++) {
        const std::string state = get_state(dish);
        loads.push_back(calculate_load(dish));
        if (visited_states.contains(state)) {
            break;
        }
        visited_states.insert(state);

        for (int j = 0; j < 4; j++) {
            tilt_north(dish);
            dish = rotate(dish);
        }
    }

    size_t start_cycle = std::distance(
        loads.begin(), std::find(loads.begin(), loads.end(), loads.back()));

    return loads[start_cycle +
                 (1000000000 - start_cycle) % (loads.size() - 2 - start_cycle)];
}

int main() {
//     std::string input = R"(O....#....
// O.OO#....#
// .....##...
// OO.#O....O
// .O.....O#.
// O.#..O.#.#
// ..O..#O..O
// .......O..
// #....###..
// #OO..#....)";
    std::string input = read_day(14);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}