#include <iostream>
#include <unordered_map>
#include <vector>

#include "utils.h"

typedef struct {
    int value;
    point_t left;
    point_t right;
} engine_number_t;

std::tuple<std::vector<engine_number_t>, std::unordered_map<std::string, char>>
parse_input(const std::string &input) {
    std::vector<engine_number_t> numbers{};
    std::unordered_map<std::string, char> symbols{};

    int row{}, col{};
    int row_len = input.find("\n");
    int last_i = 0;

    while (1) {
        int i = input.find_first_not_of(".", last_i);
        last_i = i + 1;
        if (i == -1) {
            break;
        }

        col = i - row * (row_len + 1);

        if (isdigit(input[i])) {
            while (isdigit(input[last_i])) {
                last_i++;
            }
            int value = std::stol(input.substr(i, last_i - i));
            point_t left = {.x = col, .y = row};
            point_t right = {.x = col + last_i - i - 1, .y = row};
            numbers.push_back({.value = value, .left = left, .right = right});
        } else if (input[i] == '\n') {
            row++;
            col = 0;
        } else if (isprint(input[i])) {
            point_t p = {.x = col, .y = row};
            symbols.insert(
                std::pair<std::string, char>(p.to_string(), input[i]));
        }
    }

    return std::make_tuple(numbers, symbols);
}

int part1(std::string input) {
    const auto [numbers, symbols] = parse_input(input);

    int sum = 0;

    for (const engine_number_t &n : numbers) {
        bool engine_number_found = false;
        int len = n.right.x - n.left.x + 1;
        for (int row = -1; row <= 1; row++) {
            for (int col = -1; col <= len; col++) {
                if (row == 0 && col > -1 && col < len) {
                    continue;
                }

                point_t p = {.x = n.left.x + col, .y = n.left.y + row};
                if (symbols.count(p.to_string()) > 0) {
                    sum += n.value;
                    engine_number_found = true;
                    break;
                }
            }

            if (engine_number_found) {
                break;
            }
        }
    }

    return sum;
}

int part2(std::string input) {
    const auto [numbers, symbols] = parse_input(input);

    int sum = 0;

    for (const auto &[k, v] : symbols) {
        if (v != '*') {
            continue;
        }

        int cnt = 0;
        int gear_ratio = 1;
        point_t gear = point_from_string(k);
        for (const engine_number_t &n : numbers) {
            int dxl = abs(gear.x - n.left.x);
            int dyl = abs(gear.y - n.left.y);
            int dxr = abs(gear.x - n.right.x);
            int dyr = abs(gear.y - n.right.y);

            if ((dxl <= 1 && dyl <= 1) || (dxr <= 1 && dyr <= 1)) {
                cnt++;
                if (cnt > 2) {
                    break;
                }
                gear_ratio *= n.value;
            }
        }
        if (cnt == 2) {
            sum += gear_ratio;
        }
    }

    return sum;
}

int main() {
    //     std::string input = R"(467..114..
    // ...*......
    // ..35..633.
    // ......#...
    // 617*......
    // .....+.58.
    // ..592.....
    // ......755.
    // ...$.*....
    // .664.598..)";
    std::string input = read_day(3);

    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";

    return 0;
}