#include <algorithm>
#include <cassert>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <sstream>
#include <unordered_set>
#include <vector>

#include "utils.h"

typedef struct {
    std::vector<int> values;
    size_t rows;
    size_t cols;

    int get(size_t x, size_t y) const {
        if (x >= cols || y >= rows) {
            return -1;
        }
        return values[x + y * cols];
    }

    size_t get_ix(int x, int y) const {
        if (x >= static_cast<int>(cols) || y >= static_cast<int>(rows) ||
            x < 0 || y < 0) {
            return SIZE_MAX;
        }
        return x + y * cols;
    }

    int get_x(size_t ix) const {
        if (ix >= values.size()) {
            return -1;
        }
        return ix % cols;
    }

    int get_y(size_t ix) const {
        if (ix >= values.size()) {
            return -1;
        }
        return ix / cols;
    }
} map_t;

map_t parse_input(const std::string &input) {
    std::stringstream in(input);
    std::vector<int> values{};
    size_t rows{}, cols{};
    for (std::string line; std::getline(in, line);) {
        for (const char &c : line) {
            assert(isdigit(c));
            values.push_back(c - '0');
        }

        if (cols == 0) {
            cols = line.length();
        }
        rows++;
        assert(cols == line.length());
    }

    return {.values = values, .rows = rows, .cols = cols};
}

typedef struct {
    size_t ix;
    int dx;
    int dy;
    int count;
} node_t;

void get_neighbours(const map_t &map, const node_t &current_node,
                    std::vector<node_t> &n, int min, int max) {
    int x = map.get_x(current_node.ix);
    int y = map.get_y(current_node.ix);

    if (current_node.count < max - 1) {
        size_t next_ix = map.get_ix(x + current_node.dx, y + current_node.dy);
        if (next_ix < SIZE_MAX) {
            n.push_back({.ix = next_ix,
                         .dx = current_node.dx,
                         .dy = current_node.dy,
                         .count = current_node.count + 1});
        }
    }

    if (current_node.count >= min) {
        int next_dx = current_node.dy;
        int next_dy = current_node.dx;

        assert(next_dx == 0 || next_dy == 0);

        size_t next_ix = map.get_ix(x + next_dx, y + next_dy);
        if (next_ix < SIZE_MAX) {
            n.push_back(
                {.ix = next_ix, .dx = next_dx, .dy = next_dy, .count = 0});
        }

        next_dx *= -1;
        next_dy *= -1;

        next_ix = map.get_ix(x + next_dx, y + next_dy);
        if (next_ix < SIZE_MAX) {
            n.push_back(
                {.ix = next_ix, .dx = next_dx, .dy = next_dy, .count = 0});
        }
    }
}

std::string get_hash(node_t node) {
    return std::to_string(node.ix) + "," +
           std::to_string(node.dx + node.dy * 10) + "," +
           std::to_string(node.count);
}

int min_heat_loss(const map_t &map, size_t starting_x = 0,
                  size_t starting_y = 0, int min = 0, int max = 3) {
    std::unordered_set<std::string> visited{};
    std::vector<std::pair<node_t, int>> q{};
    size_t starting_pos = map.get_ix(starting_x, starting_y);
    node_t starting_node = {.ix = starting_pos, .dx = 1, .dy = 0, .count = -1};
    q.push_back(std::make_pair(starting_node, 0));
    visited.insert(get_hash(starting_node));

    while (!q.empty()) {
        int min_dist = INT32_MAX;
        size_t ix = 0;
        for (size_t i = 0; i < q.size(); i++) {
            if (q[i].second < min_dist) {
                min_dist = q[i].second;
                ix = i;
            }
        }

        auto [u, d] = q[ix];
        q.erase(q.begin() + ix);
        if (u.ix == map.values.size() - 1) {
            return d;
        }

        std::vector<node_t> neighbours{};
        get_neighbours(map, u, neighbours, min, max);

        for (const auto n : neighbours) {
            if (!visited.contains(get_hash(n))) {
                visited.insert(get_hash(n));
                q.push_back(std::make_pair(n, d + map.values[n.ix]));
            }
        }
    }

    return -1;
}

int part1(std::string input) {
    map_t map = parse_input(input);

    return min_heat_loss(map);
}

int part2(std::string input) {
    map_t map = parse_input(input);

    return min_heat_loss(map, 0, 0, 3, 10);
}

int main() {
    //     std::string input = R"(2413432311323
    // 3215453535623
    // 3255245654254
    // 3446585845452
    // 4546657867536
    // 1438598798454
    // 4457876987766
    // 3637877979653
    // 4654967986887
    // 4564679986453
    // 1224686865563
    // 2546548887735
    // 4322674655533
    // )";
    std::string input = read_day(17);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}