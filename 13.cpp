#include <cassert>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <sstream>
#include <vector>

#include "utils.h"

typedef std::vector<std::string> pattern_t;

std::vector<pattern_t> parse_input(const std::string &input) {
    std::stringstream in(input);

    std::vector<pattern_t> patterns{};
    pattern_t current_pattern{};
    for (std::string line; std::getline(in, line);) {
        if (line.empty()) {
            patterns.push_back(current_pattern);
            current_pattern.clear();
            continue;
        }
        assert(current_pattern.size() == 0 ||
               current_pattern.back().size() == line.size());
        current_pattern.push_back(line);
    }

    if (!current_pattern.empty()) {
        patterns.push_back(current_pattern);
    }

    return patterns;
}

size_t find_horizontal_reflection_position(const pattern_t &pattern,
                                           size_t start_row = 0) {
    for (size_t i = start_row + 1; i < pattern.size(); i++) {
        bool found = true;
        for (size_t j = 0; j < i; j++) {
            if (i + j >= pattern.size()) {
                break;
            }
            if (pattern[i + j] != pattern[i - j - 1]) {
                found = false;
                break;
            }
        }
        if (found) {
            return i;
        }
    }

    return SIZE_MAX;
}

size_t match_unique_different_character(const std::string &s1,
                                        const std::string &s2) {
    if (s1.size() != s2.size()) {
        return SIZE_MAX;
    }

    bool found = false;
    size_t ix{};
    for (size_t i = 0; i < s1.size(); i++) {
        if (s1[i] != s2[i]) {
            if (!found) {
                ix = i;
                found = true;
            } else {
                return SIZE_MAX;
            }
        }
    }

    return found ? ix : SIZE_MAX;
}

std::vector<point_t> find_possible_smudges(const pattern_t &pattern) {
    std::vector<point_t> smudges{};
    for (size_t i = 1; i < pattern.size(); i++) {
        for (size_t j = 0; j < i; j++) {
            if (i + j >= pattern.size()) {
                break;
            }
            const std::string &p1 = pattern[i + j];
            const std::string &p2 = pattern[i - j - 1];
            if (p1 != p2) {
                size_t smudge_ix = match_unique_different_character(p1, p2);
                if (smudge_ix != SIZE_MAX) {
                    size_t y = i - j - 1;
                    smudges.push_back({.x = static_cast<int64_t>(smudge_ix),
                                       .y = static_cast<int64_t>(y)});
                }
                break;
            }
        }
    }

    return smudges;
}

pattern_t rotate(const pattern_t &pattern) {
    pattern_t new_pattern(pattern.front().size());
    for (const std::string &line : pattern) {
        for (size_t i = 0; i < line.size(); i++) {
            new_pattern[i] += line[i];
        }
    }
    return new_pattern;
}

size_t part1(std::string input) {
    std::vector<pattern_t> patterns = parse_input(input);

    size_t cols = 0;
    size_t rows = 0;

    for (const pattern_t &p : patterns) {
        size_t try_rows = find_horizontal_reflection_position(p);
        if (try_rows != SIZE_MAX) {
            rows += try_rows;
            continue;
        }

        size_t try_cols = find_horizontal_reflection_position(rotate(p));
        assert(try_cols != SIZE_MAX);
        cols += try_cols;
    }

    return cols + 100 * rows;
}

size_t part2(std::string input) {
    std::vector<pattern_t> patterns = parse_input(input);

    size_t cols = 0;
    size_t rows = 0;

    for (const pattern_t &p : patterns) {
        std::vector<point_t> smudges_horiz = find_possible_smudges(p);
        int c = 0;

        for (const point_t &s : smudges_horiz) {
            pattern_t new_p(p);
            new_p[s.y][s.x] = new_p[s.y][s.x] == '#' ? '.' : '#';

            size_t try_rows = find_horizontal_reflection_position(new_p, s.y);
            if (try_rows != SIZE_MAX && 2 * try_rows - s.y <= new_p.size()) {
                rows += try_rows;
                c++;
                break;
            }
        }

        if (c == 1) {
            continue;
        }

        std::vector<point_t> smudges_vert = find_possible_smudges(rotate(p));
        for (const point_t &s : smudges_vert) {
            pattern_t new_p(rotate(p));
            new_p[s.y][s.x] = new_p[s.y][s.x] == '#' ? '.' : '#';
            size_t try_cols = find_horizontal_reflection_position(new_p, s.y);

            if (try_cols != SIZE_MAX && 2 * try_cols - s.y <= new_p.size()) {
                c++;
                cols += try_cols;
                break;
            }
        }
        assert(c == 1);
    }

    return cols + 100 * rows;
}

int main() {
    //     std::string input = R"(#.##..##.
    // ..#.##.#.
    // ##......#
    // ##......#
    // ..#.##.#.
    // ..##..##.
    // #.#.##.#.

    // #...##..#
    // #....#..#
    // ..##..###
    // #####.##.
    // #####.##.
    // ..##..###
    // #....#..#)";
    std::string input = read_day(13);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";

    return 0;
}