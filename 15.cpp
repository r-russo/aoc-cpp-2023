#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

#include "utils.h"

std::vector<std::string> parse_input(const std::string &input) {
    std::vector<std::string> sequence{};
    std::stringstream in(input);

    for (std::string field; std::getline(in, field, ',');) {
        sequence.push_back(field);
    }

    return sequence;
}

int hash(const std::string &input) {
    int r = 0;

    for (const char &c : input) {
        r += static_cast<int>(c);
        r *= 17;
        r %= 256;
    }

    return r;
}

int part1(std::string input) {
    std::vector<std::string> sequence = parse_input(input);
    int result = 0;
    for (const std::string &s : sequence) {
        result += hash(s);
    }
    return result;
}

int part2(std::string input) {
    std::vector<std::string> sequence = parse_input(input);
    std::vector<std::vector<std::pair<std::string, int>>> boxes(256);
    for (const std::string &s : sequence) {
        size_t cmd_ix = s.find_first_of("=-");
        std::string label = s.substr(0, cmd_ix);
        int h = hash(label);
        char cmd = s[cmd_ix];
        auto it = std::find_if(boxes[h].begin(), boxes[h].end(),
                               [&label](auto a) { return a.first == label; });
        if (cmd == '=') {
            int focal_length = std::stoi(s.substr(cmd_ix + 1));
            if (it != boxes[h].end()) {
                *it = std::make_pair(label, focal_length);
            } else {
                boxes[h].push_back(std::make_pair(label, focal_length));
            }
        } else {
            if (it != boxes[h].end()) {
                boxes[h].erase(it);
            }
        }
    }

    int focusing_power{};

    for (size_t i = 0; i < boxes.size(); i++) {
        for (size_t j = 0; j < boxes[i].size(); j++) {
            focusing_power += (i + 1) * (j + 1) * boxes[i][j].second;
        }
    }

    return focusing_power;
}

int main() {
    // std::string input =
    // "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";
    std::string input = read_day(15);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}