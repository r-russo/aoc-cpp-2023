#include <algorithm>
#include <cassert>
#include <iostream>
#include <sstream>
#include <vector>

#include "utils.h"

typedef struct p3_t {
    int x{};
    int y{};
    int z{};
} p3_t;

typedef struct cube_t {
    p3_t origin{};
    p3_t delta{};
} cube_t;

p3_t convert_str_to_p3(const std::string &input) {
    std::stringstream in(input);
    size_t c{};
    p3_t p{};
    for (std::string field; std::getline(in, field, ',');) {
        int num = std::stoi(field);
        if (c == 0) {
            p.x = num;
        } else if (c == 1) {
            p.y = num;
        } else if (c == 2) {
            p.z = num;
        }
        c++;
    }
    return p;
}

std::vector<cube_t> parse_input(const std::string &input) {
    std::stringstream in(input);
    std::vector<cube_t> cubes{};

    for (std::string line; std::getline(in, line);) {
        size_t delim_pos = line.find("~");
        std::string origin_str = line.substr(0, delim_pos);
        std::string end_str = line.substr(delim_pos + 1);

        p3_t origin = convert_str_to_p3(origin_str);
        p3_t end = convert_str_to_p3(end_str);
        p3_t delta{};
        delta.x = end.x - origin.x + 1;
        delta.y = end.y - origin.y + 1;
        delta.z = end.z - origin.z + 1;

        assert(delta.x >= 0 && delta.y >= 0 && delta.z >= 0);
        cubes.push_back({origin, delta});
    }

    return cubes;
}

void plot_cubes(const std::vector<cube_t> cubes) {
    const int max_height = 10;
    const int max_width = 10;
    const int max_depth = 10;
    std::vector<std::string> plane_zx(max_height, std::string(max_width, '.'));
    std::vector<std::string> plane_zy(max_height, std::string(max_depth, '.'));
    std::vector<std::string> plane_xy(max_width, std::string(max_depth, '.'));

    char name = 'A';
    for (const auto &c : cubes) {
        for (int z = c.origin.z; z < c.origin.z + c.delta.z; z++) {
            if (z >= max_height) {
                continue;
            }
            for (int x = c.origin.x; x < c.origin.x + c.delta.x; x++) {
                if (x >= max_width) {
                    continue;
                }
                if (plane_zx[z][x] == '.') {
                    plane_zx[z][x] = name;
                } else {
                    plane_zx[z][x] = '?';
                }
            }

            for (int y = c.origin.y; y < c.origin.y + c.delta.y; y++) {
                if (y >= max_depth) {
                    continue;
                }
                if (plane_zy[z][y] == '.') {
                    plane_zy[z][y] = name;
                } else {
                    plane_zy[z][y] = '?';
                }
            }
        }

        name++;
    }

    std::cout << "ZX:\n";
    for (const auto &row : plane_zx) {
        std::cout << row << "\n";
    }
    std::cout << "\nZY:\n";
    for (const auto &row : plane_zy) {
        std::cout << row << "\n";
    }
}

bool intersect_cubes_top(const cube_t &a, const cube_t &b) {
    int left_a = a.origin.x;
    int right_a = left_a + a.delta.x;
    int left_b = b.origin.x;
    int right_b = left_b + b.delta.x;

    int top_a = a.origin.y;
    int bottom_a = top_a + a.delta.y;
    int top_b = b.origin.y;
    int bottom_b = top_b + b.delta.y;

    if (left_a >= right_b || left_b >= right_a) {
        return false;
    }

    if (top_a >= bottom_b || top_b >= bottom_a) {
        return false;
    }

    return true;
}

int pull_down_cubes(std::vector<cube_t> &cubes) {
    int fallen = 0;
    for (size_t i = 0; i < cubes.size(); i++) {
        cube_t &c1 = cubes[i];
        if (c1.origin.z == 1) {
            continue;
        }

        int max_height = 1;
        for (size_t j = 0; j < i; j++) {
            cube_t &c2 = cubes[j];
            if (intersect_cubes_top(c1, c2)) {
                int new_height = c2.origin.z + c2.delta.z;
                if (new_height > max_height) {
                    max_height = new_height;
                }
            }
        }
        if (c1.origin.z != max_height) {
            fallen++;
        }
        c1.origin.z = max_height;
    }
    return fallen;
}

int part1(std::string input) {
    std::vector<cube_t> cubes = parse_input(input);
    std::sort(cubes.begin(), cubes.end(), [](const cube_t &a, const cube_t &b) {
        return a.origin.z < b.origin.z;
    });
    pull_down_cubes(cubes);

    int count{};
    for (size_t i = 0; i < cubes.size(); i++) {
        std::vector<cube_t> cubes_copy = cubes;
        cubes_copy.erase(cubes_copy.begin() + i);
        if (pull_down_cubes(cubes_copy) == 0) {
            count++;
        }
    }

    return count;
}

int part2(std::string input) {
    std::vector<cube_t> cubes = parse_input(input);
    std::sort(cubes.begin(), cubes.end(), [](const cube_t &a, const cube_t &b) {
        return a.origin.z < b.origin.z;
    });
    pull_down_cubes(cubes);

    int sum{};
    for (size_t i = 0; i < cubes.size(); i++) {
        std::vector<cube_t> cubes_copy = cubes;
        cubes_copy.erase(cubes_copy.begin() + i);
        sum += pull_down_cubes(cubes_copy);
    }

    return sum;
}

int main() {
    //     std::string input = R"(1,0,1~1,2,1
    // 0,0,2~2,0,2
    // 0,2,3~2,2,3
    // 0,0,4~0,2,4
    // 2,0,5~2,2,5
    // 0,1,6~2,1,6
    // 1,1,8~1,1,9)";
    std::string input = read_day(22);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}