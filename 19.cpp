#include <cassert>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "utils.h"

typedef std::unordered_map<std::string, std::string> instruction_set_t;

typedef struct {
    int x, m, a, s;

    int get(char c) const {
        switch (c) {
        case 'x':
            return x;
        case 'm':
            return m;
        case 'a':
            return a;
        case 's':
            return s;
        }
        return -1;
    }

    void set(char c, int val) {
        switch (c) {
        case 'x':
            x = val;
            break;
        case 'm':
            m = val;
            break;
        case 'a':
            a = val;
            break;
        case 's':
            s = val;
            break;
        default:
            assert(0);
        }
    }

    int sum() const { return x + m + a + s; }
} registers_t;

std::pair<instruction_set_t, std::vector<registers_t>>
parse_input(const std::string &input) {
    std::stringstream in(input);
    bool parsing_instructions{true};

    instruction_set_t instruction_set{};
    std::vector<registers_t> register_set{};

    for (std::string line; std::getline(in, line);) {
        if (line.empty()) {
            parsing_instructions = false;
            continue;
        }

        if (parsing_instructions) {
            size_t brace = line.find('{');
            std::string key = line.substr(0, brace);
            std::string val = line.substr(brace + 1, line.length() - brace - 2);
            instruction_set.insert(std::make_pair(key, val));
        } else {
            std::stringstream ln(trim(line, "{}"));
            registers_t regs;
            for (std::string field; std::getline(ln, field, ',');) {
                size_t equal = field.find('=');
                char r = field[0];
                int val = std::stoi(field.substr(equal + 1));
                regs.set(r, val);
            }
            register_set.push_back(regs);
        }
    }

    return std::make_pair(instruction_set, register_set);
}

std::pair<std::string, std::string> split_command(const std::string &command) {
    size_t colon = command.find(':');
    if (colon == std::string::npos) {
        return std::make_pair("", command);
    }
    const std::string pred = command.substr(0, colon);
    const std::string result = command.substr(colon + 1);

    return std::make_pair(pred, result);
}

std::string parse_instruction(const std::string &instruction,
                              const registers_t r) {
    std::stringstream in(instruction);
    for (std::string command; std::getline(in, command, ',');) {
        const auto [pred, result] = split_command(command);

        if (pred == "") {
            return result;
        }

        char var = pred[0];
        char op = pred[1];
        int val = std::stoi(pred.substr(2));

        switch (op) {
        case '<':
            if (r.get(var) < val) {
                return result;
            }
            break;
        case '>':
            if (r.get(var) > val) {
                return result;
            }
            break;
        }
    }
    return "";
}

size_t explore_solution_space(const instruction_set_t &instruction_set,
                              registers_t min, registers_t max,
                              std::string workflow) {

    if (workflow == "A") {
        if (max.x >= min.x && max.m >= min.m && max.a >= min.a &&
            max.s >= min.s) {
            size_t c = static_cast<size_t>(max.x - min.x + 1) *
                       static_cast<size_t>(max.m - min.m + 1) *
                       static_cast<size_t>(max.a - min.a + 1) *
                       static_cast<size_t>(max.s - min.s + 1);
            return c;
        }
        return 0;
    } else if (workflow == "R") {
        return 0;
    }

    std::string instruction = instruction_set.at(workflow);
    std::stringstream in(instruction);

    size_t combinations{};
    for (std::string command; std::getline(in, command, ',');) {
        const auto [pred, result] = split_command(command);

        if (pred != "") {
            char var = pred[0];
            char op = pred[1];
            int val = std::stoi(pred.substr(2));

            registers_t true_max = max;
            registers_t true_min = min;

            switch (op) {
            case '<':
                true_max.set(var, val - 1);
                min.set(var, val);
                break;
            case '>':
                true_min.set(var, val + 1);
                max.set(var, val);
                break;
            default:
                assert(0);
            }

            combinations += explore_solution_space(instruction_set, true_min,
                                                   true_max, result);
        } else {
            combinations +=
                explore_solution_space(instruction_set, min, max, result);
        }
    }

    return combinations;
}

int part1(std::string input) {
    const auto [instruction_set, register_set] = parse_input(input);
    int result = 0;
    for (const auto &r : register_set) {
        std::string current_workflow = "in";
        while (current_workflow != "A" && current_workflow != "R") {
            std::string instruction = instruction_set.at(current_workflow);
            current_workflow = parse_instruction(instruction, r);
        }
        if (current_workflow == "A") {
            result += r.sum();
        }
    }

    return result;
}

size_t part2(std::string input) {
    const auto [instruction_set, register_set] = parse_input(input);
    registers_t min = {.x = 1, .m = 1, .a = 1, .s = 1};
    registers_t max = {.x = 4000, .m = 4000, .a = 4000, .s = 4000};
    return explore_solution_space(instruction_set, min, max, "in");
}

int main() {
    //     std::string input = R"(px{a<2006:qkq,m>2090:A,rfg}
    // pv{a>1716:R,A}
    // lnx{m>1548:A,A}
    // rfg{s<537:gd,x>2440:R,A}
    // qs{s>3448:A,lnx}
    // qkq{x<1416:A,crn}
    // crn{x>2662:A,R}
    // in{s<1351:px,qqz}
    // qqz{s>2770:qs,m<1801:hdj,R}
    // gd{a>3333:R,R}
    // hdj{m>838:A,pv}

    // {x=787,m=2655,a=1222,s=2876}
    // {x=1679,m=44,a=2067,s=496}
    // {x=2036,m=264,a=79,s=2244}
    // {x=2461,m=1339,a=466,s=291}
    // {x=2127,m=1623,a=2188,s=1013})";

    std::string input = read_day(19);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}