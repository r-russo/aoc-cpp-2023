#include <array>
#include <cstdint>
#include <iostream>
#include <numeric>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "utils.h"

typedef struct {
    std::set<int> winning_numbers;
    std::set<int> numbers;
} scratch_card_t;

void parse_numbers(const std::string &input, std::set<int> &numbers) {
    size_t last_ix = 0;
    size_t del_ix;
    while ((del_ix = input.find(" ", last_ix)) != std::string::npos) {
        std::string num_str = input.substr(last_ix, del_ix - last_ix);
        if (is_number(num_str)) {
            numbers.insert(std::stol(num_str));
        }
        last_ix = del_ix + 1;
    }
    numbers.insert(std::stol(input.substr(last_ix)));
}

void parse_input(const std::string &input, std::vector<scratch_card_t> &cards) {
    std::stringstream in(input);

    for (std::string line; std::getline(in, line);) {
        size_t winning_numbers_ix = line.find(":");
        size_t numbers_ix = line.find("|") + 1;

        std::string winning_numbers =
            trim(line.substr(winning_numbers_ix + 1,
                             numbers_ix - winning_numbers_ix - 2),
                 " ");
        std::string numbers = trim(line.substr(numbers_ix + 1), " ");

        scratch_card_t sc{};
        parse_numbers(winning_numbers, sc.winning_numbers);
        parse_numbers(numbers, sc.numbers);
        cards.push_back(sc);
    }
}

uint32_t part1(std::string input) {
    std::vector<scratch_card_t> cards{};
    parse_input(input, cards);

    uint32_t total = 0;
    for (const auto &card : cards) {
        uint32_t points = 0;
        for (const auto &n : card.numbers) {
            if (card.winning_numbers.count(n) > 0) {
                points = points > 0 ? points << 1 : 1;
            }
        }

        total += points;
    }

    return total;
}

uint32_t part2(std::string input) {
    std::vector<scratch_card_t> cards{};
    parse_input(input, cards);

    std::vector<int> winners{};
    for (const auto &card : cards) {
        int w{};
        for (const auto &n : card.numbers) {
            if (card.winning_numbers.count(n) > 0) {
                w++;
            }
        }
        winners.push_back(w);
    }

    size_t id = 0;
    uint32_t new_cards = cards.size();
    std::vector<int> count_cards(cards.size(), 1);

    while (id < cards.size()) {
        new_cards = winners[id];
        for (size_t i = id + 1; i < id + new_cards + 1; i++) {
            count_cards[i] += count_cards[id];
        }

        id++;
    }

    return std::reduce(count_cards.begin(), count_cards.end());
}

int main() {
    //     std::string input = R"(Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48
    //     53
    // Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
    // Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
    // Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
    // Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
    // Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11)";
    std::string input = read_day(4);

    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";

    return 0;
}