#include <cstdint>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

#include "utils.h"

class Range {
  public:
    Range(std::string dst) { this->next = dst; };

    const std::string &dst() const { return this->next; }

    void add_range(const std::string &line) {
        size_t last_ix = 0;
        size_t ix;

        std::vector<uint32_t> values{};
        while ((ix = line.find(" ", last_ix)) != std::string::npos) {
            values.push_back(std::stoul(line.substr(last_ix, last_ix - ix)));
            last_ix = ix + 1;
        }
        values.push_back(std::stoul(line.substr(last_ix)));

        uint32_t src_start = values[1];
        uint32_t dst_start = values[0];
        uint32_t length = values[2];

        src_range.push_back(std::make_pair(src_start, src_start + length));
        dst_range.push_back(std::make_pair(dst_start, src_start + length));
    }

    uint32_t output(uint32_t input) {
        size_t ix = SIZE_MAX;
        for (size_t i = 0; i < src_range.size(); i++) {
            if (input >= src_range[i].first && input < src_range[i].second) {
                ix = i;
            }
        }

        if (ix == SIZE_MAX) {
            return input;
        }

        return input - src_range[ix].first + dst_range[ix].first;
    }

  private:
    std::string next{};
    std::vector<std::pair<uint32_t, uint32_t>> src_range{};
    std::vector<std::pair<uint32_t, uint32_t>> dst_range{};
};

typedef std::vector<uint32_t> seeds_t;
typedef std::map<std::string, Range> almanac_t;

std::pair<seeds_t, almanac_t> parse_input(const std::string &input) {
    std::stringstream in(input);
    std::string line{};
    seeds_t seeds{};
    almanac_t almanac;

    std::string dst{};
    std::string src{};
    Range *current_range = nullptr;

    for (std::string line; std::getline(in, line);) {
        line = trim(line, " \r\n");
        if (line.empty() && !src.empty() && !dst.empty()) {
            if (current_range != nullptr) {
                almanac.insert(std::make_pair(src, *current_range));
                free(current_range);
                current_range = nullptr;
            }
            dst.clear();
            src.clear();
            continue;
        }

        size_t colon = line.find(":");

        if (colon != std::string::npos) {
            std::string map = line.substr(0, colon);
            if (map == "seeds") {
                std::string seeds_str = trim(line.substr(colon + 1), " ");
                size_t last_ix = 0;
                size_t ix;
                while ((ix = seeds_str.find(" ", last_ix)) !=
                       std::string::npos) {
                    uint32_t seed =
                        std::stoull(seeds_str.substr(last_ix, ix - last_ix));
                    seeds.push_back(seed);
                    last_ix = ix + 1;
                }
                uint32_t seed = std::stoull(seeds_str.substr(last_ix));
                seeds.push_back(seed);
            } else {
                std::string src_dst_str = line.substr(0, line.find(" "));
                src = src_dst_str.substr(0, line.find_first_of("-"));
                dst = src_dst_str.substr(line.find_last_of("-") + 1);
                current_range = new Range(dst);
            }
        } else {
            if (current_range == nullptr) {
                continue;
            }
            current_range->add_range(line);
        }
    }
    if (current_range != nullptr) {
        almanac.insert(std::make_pair(src, *current_range));
        free(current_range);
        current_range = nullptr;
    }
    dst.clear();
    src.clear();

    return std::make_pair(seeds, almanac);
}

uint32_t get_final_output(const almanac_t &almanac, uint32_t seed) {
    uint32_t value = seed;
    if (!almanac.contains(std::string("seed"))) {
        return UINT32_MAX;
    }
    Range r = almanac.at(std::string("seed"));
    while (r.dst() != "location") {
        value = r.output(value);
        r = almanac.at(r.dst());
    }
    return r.output(value);
}

uint32_t part1(std::string input) {
    const auto [seeds, almanac] = parse_input(input);
    uint32_t min = UINT32_MAX;

    for (const uint32_t &seed : seeds) {
        uint32_t value = get_final_output(almanac, seed);

        if (value < min) {
            min = value;
        }
    }

    return min;
}

uint32_t find_minimum_in_range(const almanac_t &almanac, uint32_t left,
                               uint32_t right) {
    uint32_t first = get_final_output(almanac, left);
    uint32_t last = get_final_output(almanac, right);
    if (abs(last - first) == abs(right - left)) {
        return first;
    }

    uint32_t center = left + (right - left) / 2;
    uint32_t min_left = find_minimum_in_range(almanac, left, center);
    uint32_t min_right = find_minimum_in_range(almanac, center + 1, right);

    return std::min(min_left, min_right);
}

uint32_t part2(std::string input) {
    const auto [seeds, almanac] = parse_input(input);
    uint32_t min = UINT32_MAX;

    for (size_t i = 0; i < seeds.size(); i += 2) {
        uint32_t start = seeds[i];
        uint32_t end = seeds[i] + seeds[i + 1] - 1;

        uint32_t value = find_minimum_in_range(almanac, start, end);
        if (value < min) {
            min = value;
        }
    }

    return min;
}

int main() {
    //     std::string input = R"(seeds: 79 14 55 13

    // seed-to-soil map:
    // 50 98 2
    // 52 50 48

    // soil-to-fertilizer map:
    // 0 15 37
    // 37 52 2
    // 39 0 15

    // fertilizer-to-water map:
    // 49 53 8
    // 0 11 42
    // 42 0 7
    // 57 7 4

    // water-to-light map:
    // 88 18 7
    // 18 25 70

    // light-to-temperature map:
    // 45 77 23
    // 81 45 19
    // 68 64 13

    // temperature-to-humidity map:
    // 0 69 1
    // 1 0 69

    // humidity-to-location map:
    // 60 56 37
    // 56 93 4)";
    std::string input = read_day(5);

    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";

    return 0;
}