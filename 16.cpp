#include <algorithm>
#include <cassert>
#include <iostream>
#include <queue>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "utils.h"

typedef struct beam_t {
    int x;
    int y;
    int dx;
    int dy;
    size_t hash() const {
        return dx + 1 + (dy + 1) * 10 + x * 1000 + y * 100 * 100000;
    };
} beam_t;

std::vector<std::string> parse_input(const std::string &input) {
    std::stringstream in(input);
    std::vector<std::string> layout{};

    for (std::string line; std::getline(in, line);) {
        layout.push_back(line);
        assert(layout.size() == 1 || line.length() == layout.back().length());
    }

    return layout;
}

int count_energized_spaces(const std::vector<std::string> &layout,
                           beam_t starting_position) {
    std::queue<beam_t> beams{};
    std::unordered_set<size_t> visited{};
    std::vector<bool> energized_spaces(layout.size() * layout[0].size());
    beams.push(starting_position);
    visited.insert(beams.back().hash());

    while (!beams.empty()) {
        auto beam = beams.front();
        beams.pop();

        if (beam.x < 0 || beam.y < 0 ||
            beam.y >= static_cast<int>(layout.size()) ||
            beam.x >= static_cast<int>(layout[beam.y].size())) {
            continue;
        }

        energized_spaces[beam.x + layout[0].size() * beam.y] = true;

        std::vector<beam_t> next{};

        switch (layout[beam.y][beam.x]) {
        case '.':
            next.push_back(beam);
            break;
        case '|':
            if (beam.dx == 0) {
                next.push_back(beam);
            } else {
                next.push_back({.x = beam.x, .y = beam.y, .dx = 0, .dy = 1});
                next.push_back({.x = beam.x, .y = beam.y, .dx = 0, .dy = -1});
            }
            break;
        case '-':
            if (beam.dy == 0) {
                next.push_back(beam);
            } else {
                next.push_back({.x = beam.x, .y = beam.y, .dx = 1, .dy = 0});
                next.push_back({.x = beam.x, .y = beam.y, .dx = -1, .dy = 0});
            }
            break;
        case '\\':
            next.push_back(
                {.x = beam.x, .y = beam.y, .dx = beam.dy, .dy = beam.dx});
            break;
        case '/':
            next.push_back(
                {.x = beam.x, .y = beam.y, .dx = -beam.dy, .dy = -beam.dx});
            break;
        default:
            assert(0);
        }

        for (auto beam : next) {
            beam.x += beam.dx;
            beam.y += beam.dy;

            if (!visited.contains(beam.hash())) {
                beams.push(beam);
                visited.insert(beam.hash());
            }
        }
    }

    return std::count(energized_spaces.begin(), energized_spaces.end(), true);
}

int part1(std::string input) {
    std::vector<std::string> layout = parse_input(input);

    return count_energized_spaces(layout, {.x = 0, .y = 0, .dx = 1, .dy = 0});
}

int part2(std::string input) {
    std::vector<std::string> layout = parse_input(input);

    int max_energized_spaces{};

    for (size_t y = 0; y < layout.size(); y++) {
        beam_t beam_left = {.x = 0, .y = static_cast<int>(y), .dx = 0, .dy = 0};

        beam_left.dx = 1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_left));
        beam_left.dx = -1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_left));
        beam_left.dx = 0;
        beam_left.dy = 1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_left));
        beam_left.dy = -1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_left));

        beam_t beam_right = {.x = static_cast<int>(layout[y].length() - 1),
                             .y = static_cast<int>(y),
                             .dx = 0,
                             .dy = 0};
        beam_right.dx = 1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_right));
        beam_right.dx = -1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_right));
        beam_right.dx = 0;
        beam_right.dy = 1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_right));
        beam_right.dy = -1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_right));
    }

    for (size_t x = 0; x < layout[0].length(); x++) {
        beam_t beam_top = {.x = static_cast<int>(x), .y = 0, .dx = 0, .dy = 0};

        beam_top.dx = 1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_top));
        beam_top.dx = -1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_top));
        beam_top.dx = 0;
        beam_top.dy = 1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_top));
        beam_top.dy = -1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_top));

        beam_t beam_bottom = {.x = static_cast<int>(x),
                              .y = static_cast<int>(layout.size()),
                              .dx = 0,
                              .dy = 0};
        beam_bottom.dx = 1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_bottom));
        beam_bottom.dx = -1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_bottom));
        beam_bottom.dx = 0;
        beam_bottom.dy = 1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_bottom));
        beam_bottom.dy = -1;
        max_energized_spaces = std::max(
            max_energized_spaces, count_energized_spaces(layout, beam_bottom));
    }

    return max_energized_spaces;
}

int main() {
    /*std::string input = R"(.|...\....
|.-.\.....
.....|-...
........|.
..........
.........\
..../.\\..
.-.-/..|..
.|....-|.\
..//.|....)";*/
    std::string input = read_day(16);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}