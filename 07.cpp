#include <algorithm>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <vector>

#include "utils.h"

typedef struct {
    std::array<int, 5> values;
    int bid;
} hand_t;

std::vector<hand_t> parse_input(const std::string &input) {
    std::stringstream in(input);
    std::vector<hand_t> hands{};
    for (std::string line; std::getline(in, line);) {
        size_t space_ix = line.find(" ");
        std::string hand_str = line.substr(0, space_ix);
        std::string bid_str = line.substr(space_ix + 1);

        hand_t hand{};
        for (size_t i = 0; i < hand_str.size(); i++) {
            char c = hand_str[i];
            int value{};
            switch (c) {
            case 'A':
                value = 12;
                break;
            case 'K':
                value = 11;
                break;
            case 'Q':
                value = 10;
                break;
            case 'J':
                value = 9;
                break;
            case 'T':
                value = 8;
                break;
            default:
                value = c - '2';
                break;
            }
            hand.values[i] = value;
            hand.bid = std::stol(bid_str);
        }
        hands.push_back(hand);
    }

    return hands;
}

int get_strength(const hand_t &hand) {
    std::unordered_map<int, int> set_of_values{};
    for (auto &v : hand.values) {
        if (set_of_values.count(v) > 0) {
            set_of_values[v]++;
        } else {
            set_of_values.insert(std::make_pair(v, 1));
        }
    }

    int prod = 1;
    switch (set_of_values.size()) {
    case 1:
        return 7;
    case 2:
        for (const auto &[k, v] : set_of_values) {
            prod *= v;
        }

        if (prod == 4) {
            return 6;
        } else {
            return 5;
        }
    case 3:
        for (const auto &[k, v] : set_of_values) {
            prod *= v;
        }

        if (prod == 3) {
            return 4;
        } else {
            return 3;
        }
    case 4:
        return 2;
    case 5:
        return 1;
    }

    return 0;
}

std::vector<hand_t> parse_input2(const std::string &input) {
    std::stringstream in(input);
    std::vector<hand_t> hands{};
    for (std::string line; std::getline(in, line);) {
        size_t space_ix = line.find(" ");
        std::string hand_str = line.substr(0, space_ix);
        std::string bid_str = line.substr(space_ix + 1);

        hand_t hand{};
        for (size_t i = 0; i < hand_str.size(); i++) {
            char c = hand_str[i];
            int value{};
            switch (c) {
            case 'A':
                value = 12;
                break;
            case 'K':
                value = 11;
                break;
            case 'Q':
                value = 10;
                break;
            case 'J':
                value = 0;
                break;
            case 'T':
                value = 9;
                break;
            default:
                value = c + 1 - '2';
                break;
            }
            hand.values[i] = value;
            hand.bid = std::stol(bid_str);
        }
        hands.push_back(hand);
    }

    return hands;
}

int get_strength2(const hand_t &hand) {
    std::unordered_map<int, int> set_of_values{};
    for (auto &v : hand.values) {
        if (set_of_values.count(v) > 0) {
            set_of_values[v]++;
        } else {
            set_of_values.insert(std::make_pair(v, 1));
        }
    }

    if (set_of_values.contains(0)) {
        int v = set_of_values[0];
        set_of_values.erase(0);

        int max = 0;
        int key_max = 0;
        for (const auto &[k, v] : set_of_values) {
            if (v > max) {
                max = v;
                key_max = k;
            }
        }
        set_of_values[key_max] += v;
    }

    int prod = 1;
    switch (set_of_values.size()) {
    case 1:
        return 7;
    case 2:
        for (const auto &[k, v] : set_of_values) {
            prod *= v;
        }

        if (prod == 4) {
            return 6;
        } else {
            return 5;
        }
    case 3:
        for (const auto &[k, v] : set_of_values) {
            prod *= v;
        }

        if (prod == 3) {
            return 4;
        } else {
            return 3;
        }
    case 4:
        return 2;
    case 5:
        return 1;
    }

    return 0;
}

int part1(std::string input) {
    auto hands = parse_input(input);
    std::vector<std::array<int, 7>> ranked_hands{};
    for (auto &hand : hands) {
        std::array<int, 7> rankable{};
        rankable[0] = get_strength(hand);

        for (size_t i = 0; i < hand.values.size(); i++) {
            rankable[i + 1] = hand.values[i];
        }
        rankable[6] = hand.bid;
        ranked_hands.push_back(rankable);
    }

    std::sort(ranked_hands.begin(), ranked_hands.end());

    int result = 0;
    for (size_t i = 0; i < ranked_hands.size(); i++) {
        result += ranked_hands[i][6] * (i + 1);
    }

    return result;
}

int part2(std::string input) {
    auto hands = parse_input2(input);
    std::vector<std::array<int, 7>> ranked_hands{};
    for (auto &hand : hands) {
        std::array<int, 7> rankable{};
        rankable[0] = get_strength2(hand);

        for (size_t i = 0; i < hand.values.size(); i++) {
            rankable[i + 1] = hand.values[i];
        }
        rankable[6] = hand.bid;
        ranked_hands.push_back(rankable);
    }

    std::sort(ranked_hands.begin(), ranked_hands.end());

    int result = 0;
    for (size_t i = 0; i < ranked_hands.size(); i++) {
        result += ranked_hands[i][6] * (i + 1);
    }

    return result;
}

int main() {
    //     std::string input = R"(32T3K 765
    // T55J5 684
    // KK677 28
    // KTJJT 220
    // QQQJA 483
    // )";
    std::string input = read_day(7);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}