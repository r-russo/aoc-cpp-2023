#include <algorithm>
#include <cassert>
#include <iostream>
#include <queue>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "utils.h"

typedef enum module_type_t { BROADCASTER, FLIP_FLOP, CONJUCTION } module_type_t;

typedef struct module_t {
    bool state;
    module_type_t type;
    std::string name;
    std::vector<std::string> outputs;
    std::unordered_map<std::string, bool> memory;
    bool memory_full;
} module_t;

typedef std::unordered_map<std::string, module_t> connections_t;

connections_t parse_input(const std::string &input) {
    std::stringstream in(input);
    connections_t connections{};

    for (std::string line; std::getline(in, line);) {
        size_t arrow = line.find(" -> ");
        std::string from = trim(line.substr(0, arrow), " &%");
        std::string str_to = trim(line.substr(arrow + 3), " ");
        std::pair<module_type_t, std::vector<std::string>> to{};
        module_t module;
        module.state = false;
        module.name = from;
        module.memory_full = false;
        switch (line[0]) {
        case '%':
            module.type = FLIP_FLOP;
            break;
        case '&':
            module.type = CONJUCTION;
            break;
        default:
            module.type = BROADCASTER;
            break;
        }

        std::stringstream ss(str_to);
        for (std::string field; std::getline(ss, field, ',');) {
            module.outputs.push_back(trim(field, " "));
        }
        connections.insert(std::make_pair(from, module));

        for (auto &[k, v] : connections) {
            if (v.type != CONJUCTION) {
                continue;
            }

            for (const auto &[k2, v2] : connections) {
                if (std::find(v2.outputs.begin(), v2.outputs.end(), k) ==
                    v2.outputs.end()) {
                    continue;
                }

                v.memory.insert(std::make_pair(k2, false));
            }
        }
    }

    return connections;
}

std::optional<bool> flip_flop(module_t &module, bool pulse) {
    if (pulse) {
        return {};
    }
    module.state = !module.state;
    return module.state;
}

std::optional<bool> conjunction(module_t &module, bool pulse,
                                const std::string &from) {
    module.memory[from] = pulse;
    for (const auto &[k, v] : module.memory) {
        if (!v) {
            return true;
        }
    }
    return false;
}

std::optional<bool> activate_module(module_t &module, bool pulse,
                                    const std::string &from = "") {
    switch (module.type) {
    case FLIP_FLOP:
        return flip_flop(module, pulse);
    case CONJUCTION:
        return conjunction(module, pulse, from);
    case BROADCASTER:
        return pulse;
    }

    return {};
}

bool push_button(connections_t &connections, int64_t &low_count,
                 int64_t &high_count) {
    std::queue<std::tuple<std::string, std::string, bool>> q;
    q.push(std::make_tuple("broadcaster", "button", false));

    int pulses_to_rx = 0;

    while (!q.empty()) {
        auto &[name, from, in_pulse] = q.front();
        q.pop();
        high_count += in_pulse ? 1 : 0;
        low_count += !in_pulse ? 1 : 0;

        if (name == "rx" && !in_pulse) {
            pulses_to_rx++;
        }

        if (!connections.contains(name)) {
            continue;
        }
        module_t &m = connections.at(name);

        std::optional<bool> out = activate_module(m, in_pulse, from);

        if (!out.has_value()) {
            continue;
        }

        bool out_pulse = out.value();

        if (m.type == CONJUCTION && !out_pulse) {
            m.memory_full = true;
        }

        for (const auto &o : m.outputs) {
            q.push(std::make_tuple(o, name, out_pulse));
        }
    }

    return pulses_to_rx == 1;
}

int64_t part1(std::string input) {
    connections_t connections = parse_input(input);
    int64_t low_count = 0;
    int64_t high_count = 0;

    for (int i = 0; i < 1000; i++) {
        push_button(connections, low_count, high_count);
    }

    return low_count * high_count;
}

int64_t part2(std::string input) {
    connections_t connections = parse_input(input);
    int64_t low_count = 0;
    int64_t high_count = 0;
    int pulses{0};
    std::unordered_set<std::string> set{};
    std::vector<int64_t> values{};
    for (int i = 0; i < 10000; i++) {
        pulses++;
        push_button(connections, low_count, high_count);
        for (auto &[k, v] : connections) {
            if (v.type != CONJUCTION) {
                continue;
            }

            if (v.memory_full && !set.contains(v.name)) {
                if (pulses > 1) {
                    values.push_back(pulses);
                }
                v.memory_full = false;
                set.insert(v.name);
            }
        }
    }

    int64_t result = 1;

    for (const auto &v : values) {
        result *= v;
    }

    return result;
}

int main() {
    //     std::string input = R"(broadcaster -> a, b, c
    // %a -> b
    // %b -> c
    // %c -> inv
    // &inv -> a)";
    //     std::string input = R"(broadcaster -> a
    // %a -> inv, con
    // &inv -> b
    // %b -> con
    // &con -> output)";
    std::string input = read_day(20);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}