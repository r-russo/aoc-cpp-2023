#include <array>
#include <iostream>
#include <sstream>
#include <string>

#include "utils.h"

int day1_part1(std::string input) {
    std::stringstream in(input);

    int sum{};
    for (std::string line; std::getline(in, line);) {
        int num{};
        for (char &c : line) {
            if (isdigit(c)) {
                num = (c - '0') * 10;
                break;
            }
        }
        for (auto it = line.end() - 1; it > line.begin() - 1; it--) {
            char c = *it;
            if (isdigit(c)) {
                num += c - '0';
                break;
            }
        }
        sum += num;
    }

    return sum;
}

int day1_part2(std::string input) {
    std::stringstream in(input);

    std::array<std::string, 9> words = {"one", "two",   "three", "four", "five",
                                        "six", "seven", "eight", "nine"};

    int sum{};
    for (std::string line; std::getline(in, line);) {
        int num1{}, num2{};
        int ix{-1};
        for (size_t i = 0; i < line.length(); i++) {
            char c = line.at(i);
            if (isdigit(c)) {
                num1 = (c - '0') * 10;
                ix = i;
                break;
            }
        }

        for (size_t i = 0; i < words.size(); i++) {
            std::string &word = words[i];
            int find_pos = line.find(word);
            if ((ix == -1 || find_pos < ix) && find_pos >= 0) {
                ix = find_pos;
                num1 = (i + 1) * 10;
            }
        }

        ix = -1;
        for (size_t i = line.length() - 1; i < line.length(); i--) {
            char c = line.at(i);
            if (isdigit(c)) {
                num2 = c - '0';
                ix = i;
                break;
            }
        }

        for (size_t i = 0; i < words.size(); i++) {
            std::string &word = words[i];
            int find_pos = line.rfind(word);
            if ((ix == -1 || find_pos > ix) && find_pos >= 0) {
                ix = find_pos;
                num2 = i + 1;
            }
        }

        int num = num1 + num2;
        sum += num;
    }

    return sum;
}

int main() {
    // std::string input = "1abc2\npqr3stu8vwx\na1b2c3d4e5f\ntreb7uchet";
    //     std::string input = R"(two1nine
    // eightwothree
    // abcone2threexyz
    // xtwone3four
    // 4nineeightseven2
    // zoneight234
    // 7pqrstsixteen)";
    std::string input = read_day(1);

    std::cout << "Part 1: " << day1_part1(input) << '\n';
    std::cout << "Part 2: " << day1_part2(input) << '\n';

    return 0;
}