#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "utils.h"

typedef struct {
    int red{};
    int blue{};
    int green{};
} cubes_set_t;

typedef std::vector<cubes_set_t> game_t;

std::vector<game_t> parse_input(const std::string &input) {
    std::stringstream in(input);
    std::vector<game_t> games{};

    for (std::string line; std::getline(in, line);) {
        size_t colon = line.find(":");
        std::string str_game = line.substr(0, colon);
        std::string str_sets = line.substr(colon + 2);

        size_t pos;
        size_t last = 0;
        game_t game;
        do {
            cubes_set_t set{};
            pos = str_sets.find(";", last);
            std::string str_cubes = str_sets.substr(last, pos - last);

            size_t pos2;
            size_t last2 = 0;
            do {
                pos2 = str_cubes.find(",", last2);

                std::string str_cube =
                    trim(str_cubes.substr(last2, pos2 - last2), " ");

                size_t space = str_cube.find(" ");
                int qty = std::stol(str_cube.substr(0, space));
                std::string color = str_cube.substr(space + 1);

                if (color == "red") {
                    set.red = qty;
                } else if (color == "green") {
                    set.green = qty;
                } else if (color == "blue") {
                    set.blue = qty;
                }

                last2 = pos2 + 1;
            } while (pos2 != std::string::npos);

            last = pos + 1;
            game.push_back(set);
        } while (pos != std::string::npos);

        games.push_back(game);
    }

    return games;
}

bool is_game_possible(game_t game) {
    for (const cubes_set_t &set : game) {
        if (set.red > 12 || set.green > 13 || set.blue > 14) {
            return false;
        }
    }
    return true;
}

int minimum_power(game_t game) {
    cubes_set_t max_set = game[0];
    for (size_t i = 1; i < game.size(); i++) {
        if (game[i].red > max_set.red) {
            max_set.red = game[i].red;
        }

        if (game[i].blue > max_set.blue) {
            max_set.blue = game[i].blue;
        }

        if (game[i].green > max_set.green) {
            max_set.green = game[i].green;
        }
    }

    return max_set.red * max_set.green * max_set.blue;
}

int part1(const std::string &input) {
    std::vector<game_t> games = parse_input(input);

    int sum_ids = 0;
    for (size_t i = 0; i < games.size(); i++) {
        int game_id = i + 1;
        if (is_game_possible(games[i])) {
            sum_ids += game_id;
        }
    }

    return sum_ids;
}

int part2(const std::string &input) {
    std::vector<game_t> games = parse_input(input);

    int sum_power = 0;
    for (size_t i = 0; i < games.size(); i++) {
        sum_power += minimum_power(games[i]);
    }

    return sum_power;
}

int main() {
    // std::string input =
    //     R"(Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
    // Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
    // Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
    // Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
    // Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green)";
    std::string input = read_day(2);

    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
}