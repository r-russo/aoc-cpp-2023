#include <cassert>
#include <cstdint>
#include <iostream>
#include <memory>
#include <queue>
#include <sstream>
#include <unordered_set>
#include <vector>

#include "utils.h"

typedef struct garden_t {
    std::vector<bool> cells{};
    size_t rows{};
    size_t cols{};

    bool get(int x, int y) const {
        if (x >= static_cast<int>(cols) || y >= static_cast<int>(rows) ||
            x < 0 || y < 0) {
            return false;
        }
        return cells[cols * y + x];
    }

    size_t get_ix(int x, int y) const {
        if (x >= static_cast<int>(cols) || y >= static_cast<int>(rows) ||
            x < 0 || y < 0) {
            return SIZE_MAX;
        }

        return cols * y + x;
    }
} garden_t;

std::pair<std::pair<int, int>, garden_t> parse_input(const std::string &input) {
    std::stringstream in(input);
    size_t rows{}, cols{};
    std::vector<bool> cells{};
    int starting_x{}, starting_y{};

    size_t x{}, y{};

    for (std::string line; std::getline(in, line);) {
        for (const char &c : line) {
            cells.push_back(c != '#');
            if (c == 'S') {
                starting_x = x;
                starting_y = y;
            }
            x++;
        }
        assert(cols == 0 || cols == x);
        cols = x;
        y++;
        x = 0;
    }

    rows = y;
    garden_t g = {cells, rows, cols};
    return std::make_pair(std::make_pair(starting_x, starting_y), g);
}

size_t count_steps(const garden_t &garden, int start_x, int start_y,
                   int steps) {
    std::unordered_set<std::string> visited{};
    std::unique_ptr<std::queue<std::pair<int, int>>> q1 =
        std::make_unique<std::queue<std::pair<int, int>>>();
    std::unique_ptr<std::queue<std::pair<int, int>>> q2 =
        std::make_unique<std::queue<std::pair<int, int>>>();
    q1->push(std::make_pair(start_x, start_y));
    size_t last = 0;

    for (int i = 0; i < steps; i++) {
        while (!q1->empty()) {
            auto [x, y] = q1->front();
            q1->pop();

            for (int dx = -1; dx <= 1; dx++) {
                for (int dy = -1; dy <= 1; dy++) {
                    if (dx == dy || dx == -dy) {
                        continue;
                    }
                    int nx = x + dx;
                    int ny = y + dy;
                    int cx = nx % static_cast<int>(garden.cols);
                    int cy = ny % static_cast<int>(garden.rows);

                    if (cx < 0) {
                        cx += garden.cols;
                    }
                    if (cy < 0) {
                        cy += garden.rows;
                    }
                    std::string p =
                        std::to_string(x + dx) + "," + std::to_string(y + dy);
                    if (garden.get(cx, cy) && !visited.contains(p)) {
                        q2->push(std::make_pair(nx, ny));
                        visited.insert(p);
                    }
                }
            }
        }
        q1.swap(q2);
        visited.clear();
        std::cout << i + 1 << ", " << q1->size()
                  << " diff: " << q1->size() - last << "\n";
        last = q1->size();
    }

    return q1->size();
}

int part1(std::string input) {
    auto [s, garden] = parse_input(input);
    auto [sx, sy] = s;

    return count_steps(garden, sx, sy, 64);
}

int part2(std::string input) {
    auto [s, garden] = parse_input(input);
    auto [sx, sy] = s;

    return count_steps(garden, sx, sy, 5000);
}

int main() {
    //     std::string input = R"(...........
    // .....###.#.
    // .###.##..#.
    // ..#.#...#..
    // ....#.#....
    // .##..S####.
    // .##..#...#.
    // .......##..
    // .##.#.####.
    // .##..##.##.
    // ...........)";
    std::string input = read_day(21);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}