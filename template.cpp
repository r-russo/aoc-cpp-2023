#include <iostream>

#include "utils.h"

void parse_input(const std::string &input) {}

int part1(std::string input) {
    (void)(input);
    return 0;
}

int part2(std::string input) {
    (void)(input);
    return 0;
}

int main() {
    // std::string input = read_day(10);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}