#include "utils.h"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <sstream>

point_t point_from_string(const std::string &p) {
    size_t i = p.find(",");
    int64_t x = std::stol(p.substr(0, i));
    int64_t y = std::stol(p.substr(i + 1, std::string::npos));

    return {.x = x, .y = y};
}

direction_t char_to_direction(char c) {
    switch (c) {
    case 'R':
        return EAST;
    case 'L':
        return WEST;
    case 'U':
        return NORTH;
    case 'D':
        return SOUTH;
    default:
        return NOT_VALID;
    }
}

std::string read_day(int day) {
    std::stringstream fname;
    fname << "../inputs/" << std::setfill('0') << std::setw(2) << day << ".txt";
    std::ifstream f(fname.str());
    std::stringstream out;
    out << f.rdbuf();

    return out.str();
}

std::string trim(const std::string &in, const std::string &chars) {
    size_t start = in.find_first_not_of(chars);
    size_t end = in.find_last_not_of(chars);

    if (start == std::string::npos) {
        return "";
    }

    return in.substr(start, end - start + 1);
}

bool is_number(const std::string &s) {
    return !s.empty() && std::find_if(s.begin(), s.end(), [](unsigned char c) {
                             return !std::isdigit(c);
                         }) == s.end();
}

bool operator==(const point_t &lhs, const point_t &rhs) {
    return lhs.x == rhs.x && lhs.x == rhs.y;
}