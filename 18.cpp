#include <cassert>
#include <cstdint>
#include <iostream>
#include <queue>
#include <sstream>
#include <unordered_set>
#include <vector>

#include "utils.h"

typedef struct {
    int r;
    int g;
    int b;
} color_t;

typedef struct {
    direction_t direction;
    int length;
    std::string color;
} instructions_t;

std::vector<instructions_t> parse_input(const std::string &input) {
    std::stringstream in(input);
    std::vector<instructions_t> instructions{};

    for (std::string line; std::getline(in, line);) {
        std::stringstream ln(line);
        size_t field_num{};
        char direction{};
        int length{};
        std::string color{};
        for (std::string field; std::getline(ln, field, ' ');) {
            field_num++;

            switch (field_num) {
            case 1:
                direction = field[0];
                break;
            case 2:
                length = std::stoi(field);
                break;
            case 3:
                color = trim(field, "()#");
                break;
            default:
                assert(0);
            }
        }

        instructions.push_back({.direction = char_to_direction(direction),
                                .length = length,
                                .color = color});
    }

    return instructions;
}

std::vector<point_t>
polygon_vertices(const std::vector<instructions_t> &instructions) {
    point_t p = {.x = 0, .y = 0};
    std::vector<point_t> vertices{};
    vertices.push_back(p);
    for (const instructions_t &i : instructions) {
        int dx{}, dy{};
        switch (i.direction) {
        case NORTH:
            dy = -1;
            break;
        case EAST:
            dx = 1;
            break;
        case SOUTH:
            dy = 1;
            break;
        case WEST:
            dx = -1;
            break;
        case NOT_VALID:
            assert(0);
        }
        p.x += i.length * dx;
        p.y += i.length * dy;

        vertices.push_back(p);
    }

    return vertices;
}

int64_t determinant(const point_t &p1, const point_t &p2) {
    return p1.x * p2.y - p2.x * p1.y;
}

int64_t get_polygon_area(const std::vector<point_t> &vertices) {
    int64_t area = 0;
    size_t n = vertices.size();
    for (size_t i = 0; i < n - 1; i++) {
        area += determinant(vertices[i], vertices[i + 1]);
    }
    area += determinant(vertices[n - 1], vertices[0]);
    return area / 2;
}

int64_t get_polygon_perimeter(const std::vector<point_t> &vertices) {
    int64_t perimeter = 0;
    size_t n = vertices.size();
    for (size_t i = 0; i < n - 1; i++) {
        perimeter += std::abs(vertices[i].x - vertices[i + 1].x) +
                     std::abs(vertices[i].y - vertices[i + 1].y);
    }
    perimeter += std::abs(vertices[n - 1].x - vertices[0].x) +
                 std::abs(vertices[n - 1].y - vertices[0].y);

    return perimeter;
}

int64_t part1(std::string input) {
    std::vector<instructions_t> instructions = parse_input(input);
    auto vertices = polygon_vertices(instructions);

    return get_polygon_area(vertices) + get_polygon_perimeter(vertices) / 2 + 1;
}

int64_t part2(std::string input) {
    std::vector<instructions_t> instructions = parse_input(input);

    for (instructions_t &i : instructions) {
        std::string new_length = i.color.substr(0, i.color.length() - 1);
        char new_direction = i.color.back();

        switch (new_direction) {
        case '0':
            i.direction = EAST;
            break;
        case '1':
            i.direction = SOUTH;
            break;
        case '2':
            i.direction = WEST;
            break;
        case '3':
            i.direction = NORTH;
            break;
        default:
            assert(0);
        }

        i.length = std::stoi(new_length, nullptr, 16);
    }

    auto vertices = polygon_vertices(instructions);

    return get_polygon_area(vertices) + get_polygon_perimeter(vertices) / 2 + 1;
}

int main() {
    //     std::string input = R"(R 6 (#70c710)
    // D 5 (#0dc571)
    // L 2 (#5713f0)
    // D 2 (#d2c081)
    // R 2 (#59c680)
    // D 2 (#411b91)
    // L 5 (#8ceee2)
    // U 2 (#caa173)
    // L 1 (#1b58a2)
    // U 2 (#caa171)
    // R 2 (#7807d2)
    // U 3 (#a77fa3)
    // L 2 (#015232)
    // U 2 (#7a21e3))";
    std::string input = read_day(18);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}