#include <cassert>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "utils.h"

typedef std::pair<std::string, std::vector<size_t>> row_t;

std::vector<row_t> parse_input(const std::string &input) {
    std::stringstream in(input);
    std::vector<row_t> rows;

    for (std::string line; std::getline(in, line);) {
        size_t sep = line.find(" ");
        const std::string row = line.substr(0, sep);
        std::vector<size_t> damaged_count{};
        std::stringstream ss_damaged(line.substr(sep + 1));
        for (std::string field; std::getline(ss_damaged, field, ',');) {
            damaged_count.push_back(std::stoi(field));
        }
        rows.push_back(std::make_pair(row, damaged_count));
    }

    return rows;
}

std::string get_cache_key(const std::string &row,
                          const std::vector<size_t> &damaged_count) {
    std::string damaged_str{};
    for (const size_t &v : damaged_count) {
        damaged_str += std::to_string(v) + ",";
    }
    return row + " " + damaged_str;
}

long count_possible_rows(const std::string &row,
                         const std::vector<size_t> &damaged_count,
                         std::unordered_map<std::string, long> &cache,
                         size_t pos = 0) {

    std::string key = get_cache_key(row, damaged_count);
    if (cache.contains(key)) {
        return cache.at(key);
    }

    if (pos >= row.size()) {
        if (damaged_count.size() == 1 && damaged_count.front() == row.size()) {
            return 1;
        }

        if (damaged_count.empty() && row.empty()) {
            return 1;
        }
        return 0;
    }

    char c = row[pos];

    switch (c) {
    case '.': {
        std::string row_left = trim(row.substr(0, pos), ".");
        std::string row_right = trim(row.substr(pos + 1), ".");

        if (row_left.empty()) {
            long count = count_possible_rows(row_right, damaged_count, cache);
            cache.insert(
                std::make_pair(get_cache_key(row_right, damaged_count), count));
            return count;
        }

        bool left_is_valid =
            !damaged_count.empty() && row_left.size() == damaged_count.front();
        if (!left_is_valid) {
            return 0;
        }

        std::vector<size_t> damaged_count_right(damaged_count.begin() + 1,
                                                damaged_count.end());
        long count = count_possible_rows(row_right, damaged_count_right, cache);
        cache.insert(std::make_pair(
            get_cache_key(row_right, damaged_count_right), count));

        return count;
    }
    case '#': {
        size_t next_pos = std::min(row.find("?"), row.find("."));
        return count_possible_rows(row, damaged_count, cache, next_pos);
    }
    case '?': {
        std::string row_with_operational(row);
        row_with_operational[pos] = '.';
        std::string row_with_damaged(row);
        row_with_damaged[pos] = '#';

        long count_with_operational =
            count_possible_rows(row_with_operational, damaged_count, cache);

        long count_with_damaged =
            count_possible_rows(row_with_damaged, damaged_count, cache);

        return count_with_operational + count_with_damaged;
    }
    }

    assert(0);

    return 0;
}

row_t unfold(const row_t &spring) {
    const auto &[row, damaged_count] = spring;

    std::string new_groups(row);
    std::vector<size_t> new_damaged_count{};
    new_damaged_count.insert(new_damaged_count.end(), damaged_count.begin(),
                             damaged_count.end());
    for (size_t i = 0; i < 4; i++) {
        new_groups += "?" + row;
        new_damaged_count.insert(new_damaged_count.end(), damaged_count.begin(),
                                 damaged_count.end());
    }

    return std::make_pair(new_groups, new_damaged_count);
}

long part1(std::string input) {
    std::vector<row_t> springs = parse_input(input);
    long sum = 0;
    std::unordered_map<std::string, long> cache;
    for (const auto &s : springs) {
        long count = count_possible_rows(trim(s.first, "."), s.second, cache);
        sum += count;
    }

    return sum;
}

long part2(std::string input) {
    std::vector<row_t> springs = parse_input(input);
    long sum = 0;
    std::unordered_map<std::string, long> cache;

    size_t ix = 1;
    for (const auto &s : springs) {
        row_t new_s = unfold(s);
        long count = count_possible_rows(new_s.first, new_s.second, cache);
        sum += count;
        std::cout << "[" << ix++ << "/" << springs.size() << "]" << s.first
                  << " " << count << "\n";
    }

    return sum;
}

int main() {
    //     std::string input = R"(???.### 1,1,3
    // .??..??...?##. 1,1,3
    // ?#?#?#?#?#?#?#? 1,3,1,6
    // ????.#...#... 4,1,1
    // ????.######..#####. 1,6,5
    // ?###???????? 3,2,1)";
    std::string input = read_day(12);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}