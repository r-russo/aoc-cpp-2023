#include <array>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "utils.h"

#define NORTH 0b01000
#define EAST 0b00100
#define SOUTH 0b00010
#define WEST 0b00001
#define VISITED 0b10000

std::pair<point_t, std::unordered_map<std::string, int>>
parse_input(const std::string &input) {
    int row = 0;
    int col = 0;
    point_t start{};
    std::unordered_map<std::string, int> map{};
    for (const char &c : input) {
        if (c == '\n') {
            row++;
            col = 0;
            continue;
        }
        point_t p = {.x = col, .y = row};
        int directions{};
        switch (c) {
        case '|':
            directions = NORTH | SOUTH;
            break;
        case '-':
            directions = EAST | WEST;
            break;
        case 'L':
            directions = NORTH | EAST;
            break;
        case 'J':
            directions = NORTH | WEST;
            break;
        case '7':
            directions = SOUTH | WEST;
            break;
        case 'F':
            directions = SOUTH | EAST;
            break;
        case '.':
            directions = 0;
            break;
        case 'S':
            directions = SOUTH | EAST | NORTH | WEST;
            start = p;
            break;
        default:
            break;
        }
        map.insert(std::make_pair(p.to_string(), directions));
        col++;
    }

    return std::make_pair(start, map);
}

point_t get_next_pipe(point_t p,
                      const std::unordered_map<std::string, int> &map) {
    if (!map.contains(p.to_string())) {
        return {.x = -1, .y = -1};
    }
    int directions = map.at(p.to_string());

    point_t next = {.x = p.x, .y = p.y - 1};
    if (directions & NORTH && map.contains(next.to_string())) {
        int next_directions = map.at(next.to_string());
        if (next_directions & SOUTH && !(next_directions & VISITED)) {
            return next;
        }
    }

    next = {.x = p.x, .y = p.y + 1};
    if (directions & SOUTH && map.contains(next.to_string())) {
        int next_directions = map.at(next.to_string());
        if (next_directions & NORTH && !(next_directions & VISITED)) {
            return next;
        }
    }

    next = {.x = p.x + 1, .y = p.y};
    if (directions & EAST && map.contains(next.to_string())) {
        int next_directions = map.at(next.to_string());
        if (next_directions & WEST && !(next_directions & VISITED)) {
            return next;
        }
    }

    next = {.x = p.x - 1, .y = p.y};
    if (directions & WEST && map.contains(next.to_string())) {
        int next_directions = map.at(next.to_string());
        if (next_directions & EAST && !(next_directions & VISITED)) {
            return next;
        }
    }

    return {.x = -1, .y = -1};
}

std::pair<point_t, point_t>
get_starting_directions(point_t start,
                        std::unordered_map<std::string, int> &map) {
    std::vector<point_t> next_positions{};
    point_t p{};

    map[start.to_string()] = VISITED | NORTH;
    p = get_next_pipe(start, map);
    if (p.x >= 0 && p.y >= 0) {
        next_positions.push_back(p);
    }

    map[start.to_string()] = VISITED | SOUTH;
    p = get_next_pipe(start, map);
    if (p.x >= 0 && p.y >= 0) {
        next_positions.push_back(p);
    }

    map[start.to_string()] = VISITED | EAST;
    p = get_next_pipe(start, map);
    if (p.x >= 0 && p.y >= 0) {
        next_positions.push_back(p);
    }

    map[start.to_string()] = VISITED | WEST;
    p = get_next_pipe(start, map);
    if (p.x >= 0 && p.y >= 0) {
        next_positions.push_back(p);
    }

    map[next_positions[0].to_string()] |= VISITED;
    map[next_positions[1].to_string()] |= VISITED;

    return std::make_pair(next_positions[0], next_positions[1]);
}

int part1(std::string input) {
    auto [start, map] = parse_input(input);
    auto [n1, n2] = get_starting_directions(start, map);

    int steps = 1;
    while (n1.to_string() != n2.to_string()) {
        n1 = get_next_pipe(n1, map);
        n2 = get_next_pipe(n2, map);
        map[n1.to_string()] |= VISITED;
        map[n2.to_string()] |= VISITED;
        steps++;
    }

    return steps;
}

int part2(std::string input) {
    auto [start, map] = parse_input(input);
    auto [n1, n2] = get_starting_directions(start, map);

    std::unordered_set<std::string> loop{};
    loop.insert(start.to_string());
    loop.insert(n1.to_string());
    loop.insert(n2.to_string());
    while (n1.to_string() != n2.to_string()) {
        n1 = get_next_pipe(n1, map);
        n2 = get_next_pipe(n2, map);
        map[n1.to_string()] |= VISITED;
        map[n2.to_string()] |= VISITED;
        loop.insert(n1.to_string());
        loop.insert(n2.to_string());
    }

    int x = 0;
    int y = 0;
    int count = 0;
    for (const char &c : input) {
        point_t current = {.x = x, .y = y};
        if (c == '\n') {
            y++;
            x = 0;
            continue;
        } else if (loop.count(current.to_string()) > 0) {
            x++;
            continue;
        }

        int walls = 0;
        for (int d = 0; d < x; d++) {
            point_t p = {.x = d, .y = y};
            std::string k = p.to_string();
            int directions = map.at(k);
            if (loop.count(k) > 0 &&
                ((directions & (NORTH | WEST)) != (NORTH | WEST)) &&
                ((directions & (NORTH | EAST)) != (NORTH | EAST)) &&
                ((directions & (EAST | WEST)) != (EAST | WEST))) {
                walls++;
            }
        }
        if (walls % 2 == 1) {
            count++;
        }
        x++;
    }

    return count;
}

int main() {
    //     std::string input = R"(.....
    // .S-7.
    // .|.|.
    // .L-J.
    // .....)";
    //     std::string input = R"(7-F7-
    // .FJ|7
    // SJLL7
    // |F--J
    // LJ.LJ)";
    //     std::string input = R"(...........
    // .S-------7.
    // .|F-----7|.
    // .||.....||.
    // .||.....||.
    // .|L-7.F-J|.
    // .|..|.|..|.
    // .L--J.L--J.
    // ...........)";
    //     std::string input = R"(.F----7F7F7F7F-7....
    // .|F--7||||||||FJ....
    // .||.FJ||||||||L7....
    // FJL7L7LJLJ||LJ.L-7..
    // L--J.L7...LJS7F-7L7.
    // ....F-J..F7FJ|L7L7L7
    // ....L7.F7||L7|.L7L7|
    // .....|FJLJ|FJ|F7|.LJ
    // ....FJL-7.||.||||...
    // ....L---J.LJ.LJLJ...)";
    //     std::string input = R"(FF7FSF7F7F7F7F7F---7
    // L|LJ||||||||||||F--J
    // FL-7LJLJ||||||LJL-77
    // F--JF--7||LJLJ7F7FJ-
    // L---JF-JLJ.||-FJLJJ7
    // |F|F-JF---7F7-L7L|7|
    // |FFJF7L7F-JF7|JL---7
    // 7-L-JL7||F7|L7F-7F7|
    // L.L7LFJ|||||FJL7||LJ
    // L7JLJL-JLJLJL--JLJ.L)";
    std::string input = read_day(10);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}