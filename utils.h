#pragma once

#include <format>

typedef struct point_t {
    int64_t x;
    int64_t y;

    std::string to_string() const { return std::format("{},{}", x, y); }
} point_t;

bool operator==(const point_t &lhs, const point_t &rhs);

point_t point_from_string(const std::string &p);

typedef enum { NORTH, EAST, SOUTH, WEST, NOT_VALID } direction_t;

direction_t char_to_direction(char c);

std::string read_day(int day);
std::string trim(const std::string &in, const std::string &chars);
bool is_number(const std::string &s);