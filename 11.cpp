#include <algorithm>
#include <cstdint>
#include <iostream>
#include <sstream>
#include <vector>

#include "utils.h"

typedef std::vector<point_t> galaxies_t;

galaxies_t parse_input(const std::string &input) {
    std::stringstream in(input);

    galaxies_t galaxies_location{};

    int row = 0;
    int col = 0;
    for (std::string line; std::getline(in, line);) {
        for (const char &c : line) {
            if (c == '#') {
                galaxies_location.push_back({.x = col, .y = row});
            }
            col++;
        }
        row++;
        col = 0;
    }

    return galaxies_location;
}

int64_t distance(point_t p1, point_t p2) {
    return std::abs(p1.x - p2.x) + std::abs(p1.y - p2.y);
}

point_t get_space_dims(const galaxies_t &galaxies) {
    int64_t max_x = 0;
    int64_t max_y = 0;
    for (const point_t &p : galaxies) {
        if (p.x > max_x) {
            max_x = p.x;
        }

        if (p.y > max_y) {
            max_y = p.y;
        }
    }

    return {.x = max_x, .y = max_y};
}

void expand_space(galaxies_t &galaxies, int64_t expansion_size = 1) {
    point_t dims = get_space_dims(galaxies);

    int64_t row = 0;
    while (row <= dims.y) {
        auto it = std::find_if(galaxies.begin(), galaxies.end(),
                               [&row](point_t p1) { return p1.y == row; });
        if (it == galaxies.end()) {
            dims.y += expansion_size;
            for (size_t i = 0; i < galaxies.size(); i++) {
                if (galaxies[i].y > row) {
                    galaxies[i].y += expansion_size;
                }
            }
            row += expansion_size;
        }

        row++;
    }

    int64_t col = 0;
    while (col <= dims.x) {
        auto it = std::find_if(galaxies.begin(), galaxies.end(),
                               [&col](point_t p1) { return p1.x == col; });
        if (it == galaxies.end()) {
            dims.x += expansion_size;
            for (size_t i = 0; i < galaxies.size(); i++) {
                if (galaxies[i].x > col) {
                    galaxies[i].x += expansion_size;
                }
            }
            col += expansion_size;
        }

        col++;
    }
}

int part1(std::string input) {
    galaxies_t galaxies = parse_input(input);

    expand_space(galaxies);

    int result = 0;

    for (size_t i = 0; i < galaxies.size(); i++) {
        for (size_t j = i + 1; j < galaxies.size(); j++) {
            result += distance(galaxies[i], galaxies[j]);
        }
    }

    return result;
}

int64_t part2(std::string input) {
    galaxies_t galaxies = parse_input(input);

    expand_space(galaxies, 1000000 - 1);

    int64_t result = 0;

    for (size_t i = 0; i < galaxies.size(); i++) {
        for (size_t j = i + 1; j < galaxies.size(); j++) {
            result += distance(galaxies[i], galaxies[j]);
        }
    }

    return result;
}

int main() {
    //     std::string input = R"(...#......
    // .......#..
    // #.........
    // ..........
    // ......#...
    // .#........
    // .........#
    // ..........
    // .......#..
    // #...#.....)";
    std::string input = read_day(11);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}