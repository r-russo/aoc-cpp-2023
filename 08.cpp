#include <iostream>
#include <numeric>
#include <sstream>
#include <unordered_map>
#include <vector>

#include "utils.h"

typedef std::unordered_map<std::string, std::pair<std::string, std::string>>
    nodes_t;

std::pair<std::string, nodes_t> parse_input(const std::string &input) {
    std::stringstream in(input);

    std::string instructions;
    std::getline(in, instructions);

    nodes_t nodes{};

    for (std::string line; std::getline(in, line);) {
        if (line.empty()) {
            continue;
        }

        size_t eq_ix = line.find("=");
        std::string start = line.substr(0, 3);
        std::string left = line.substr(eq_ix + 3, 3);
        std::string right = line.substr(eq_ix + 8, 3);

        nodes.insert(std::make_pair(start, std::make_pair(left, right)));
    }

    return std::make_pair(instructions, nodes);
}

int part1(std::string input) {
    const auto [instructions, nodes] = parse_input(input);

    std::string current_node = "AAA";
    size_t ix = 0;
    int steps = 0;

    while (current_node != "ZZZ") {
        char i = instructions[ix++];
        if (ix >= instructions.size()) {
            ix = 0;
        }

        steps++;
        if (i == 'L') {
            current_node = nodes.at(current_node).first;
        } else {
            current_node = nodes.at(current_node).second;
        }
    }

    return steps;
}

int64_t part2(std::string input) {
    const auto [instructions, nodes] = parse_input(input);

    std::vector<std::string> current_nodes{};
    std::vector<int> list_steps{};
    for (const auto &[n, _] : nodes) {
        if (n.ends_with('A')) {
            current_nodes.push_back(n);
        }
    }

    for (const std::string &n : current_nodes) {
        std::string current_node = n;
        int steps = 0;
        size_t ix = 0;
        while (!current_node.ends_with('Z')) {
            char i = instructions[ix++];
            if (ix >= instructions.size()) {
                ix = 0;
            }

            steps++;
            if (i == 'L') {
                current_node = nodes.at(current_node).first;
            } else {
                current_node = nodes.at(current_node).second;
            }
        }
        list_steps.push_back(steps);
    }

    int64_t steps = 1;
    for (const int &s : list_steps) {
        steps = std::lcm(steps, s);
    }

    return steps;
}

int main() {
    //     std::string input = R"(RL

    // AAA = (BBB, CCC)
    // BBB = (DDD, EEE)
    // CCC = (ZZZ, GGG)
    // DDD = (DDD, DDD)
    // EEE = (EEE, EEE)
    // GGG = (GGG, GGG)
    // ZZZ = (ZZZ, ZZZ))";
    //     std::string input = R"(LLR

    // AAA = (BBB, BBB)
    // BBB = (AAA, ZZZ)
    // ZZZ = (ZZZ, ZZZ)
    // )";
    //     std::string input = R"(LR

    // 11A = (11B, XXX)
    // 11B = (XXX, 11Z)
    // 11Z = (11B, XXX)
    // 22A = (22B, XXX)
    // 22B = (22C, 22C)
    // 22C = (22Z, 22Z)
    // 22Z = (22B, 22B)
    // XXX = (XXX, XXX))";
    std::string input = read_day(8);
    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";
    return 0;
}