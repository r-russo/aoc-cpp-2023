#include <cmath>
#include <cstdint>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "utils.h"

std::vector<std::pair<int, int>> parse_input(const std::string &input) {
    std::vector<std::pair<int, int>> races{};
    std::stringstream in(input);
    std::string line;

    std::vector<std::vector<int>> data{};

    for (int i = 0; i < 2; i++) {
        std::getline(in, line);

        std::stringstream ln(line);
        std::vector<int> row{};
        for (std::string field; std::getline(ln, field, ' ');) {
            if (!is_number(field)) {
                continue;
            }

            row.push_back(std::stol(field));
        }

        data.push_back(row);
    }

    size_t n_races =
        std::min(data.begin(), data.end(), [](const auto &a, const auto &b) {
            return a->size() < b->size();
        })->size();

    for (size_t i = 0; i < n_races; i++) {
        races.push_back(std::make_pair(data[0][i], data[1][i]));
    }

    return races;
}

int part1(std::string input) {
    auto races = parse_input(input);

    int result = 1;

    for (const auto &race : races) {
        const int time = race.first;
        const int distance = race.second;

        float disc = sqrt(time * time - 4 * distance);
        float root0 = (time - disc) / 2;
        float root1 = (time + disc) / 2;
        int h0 =
            fabs(ceil(root0) - root0) < 0.01 ? ceil(root0) + 1 : ceil(root0);
        int h1 =
            fabs(root1 - floor(root1)) < 0.01 ? floor(root1) - 1 : floor(root1);

        result *= (h1 - h0 + 1);
    }

    return result;
}

int part2(std::string input) {
    auto races = parse_input(input);

    std::string str_time = "";
    std::string str_distance = "";

    for (const auto &race : races) {
        str_time += std::to_string(race.first);
        str_distance += std::to_string(race.second);
    }

    const float time = std::stoull(str_time);
    const float distance = std::stoull(str_distance);

    float disc = sqrt(time * time - 4 * distance);
    float root0 = (time - disc) / 2;
    float root1 = (time + disc) / 2;
    int h0 = fabs(ceil(root0) - root0) < 0.01 ? ceil(root0) + 1 : ceil(root0);
    int h1 =
        fabs(root1 - floor(root1)) < 0.01 ? floor(root1) - 1 : floor(root1);

    return (h1 - h0 + 1);
}

int main() {
    //     std::string input = R"(Time:      7  15   30
    // Distance:  9  40  200)";
    std::string input = read_day(6);

    std::cout << "Part 1: " << part1(input) << "\n";
    std::cout << "Part 2: " << part2(input) << "\n";

    return 0;
}